from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool
import datetime
import math
from decimal import Decimal

__all__ = ['Bolletta']



class Bolletta(ModelSQL, ModelView):
    'Bolletta'
    __name__ = 'tributi.bolletta'
    tipo = fields.Selection([
        ('commerciale', 'Commerciale'),
        ('mortuaria', 'Mortuaria'),
        ('sociale', 'Sociale'),
        ('istituzionale', 'Istituzionale')
    ], 'tipo')
    
    party = fields.Many2One('party.party', 'Party', required=True, select=True,)

    dataCommissione = fields.Date('Data Commissione')
    dataInizio = fields.Date('Data Inizio', required=True)
    dataFine = fields.Date('Data Fine', required=True)

    @classmethod
    def default_dataInizio(cls):
        return datetime.date.today()

    @classmethod
    def default_dataFine(cls):
        return datetime.date.today()
    
    @classmethod
    def default_dataCommissione(cls):
        return datetime.date.today()


    urgenza = fields.Function(fields.Boolean('urgenza ?'), 'on_change_with_urgenza')

    numFogli = fields.Function(fields.Integer('numero fogli'), 'on_change_with_numFogli')

    @fields.depends('lines')
    def on_change_with_numFogli(self, name=None):
        if len(self.lines) > 0:
            numFogli = 0
            for line in self.lines:
                numFogli += line.on_change_with_numeroFogli()
                return numFogli
        return 0
        

    @fields.depends('tipo', 'dataInizio', 'dataCommissione')
    def on_change_with_urgenza(self, name=None):
        if self.tipo and self.dataInizio and self.dataCommissione: #questo if sembra inutile ma senza sono tutti NoneType
            tipo = self.tipo
            dataInizio = self.dataInizio
            dataCommissione = self.dataCommissione
            periodo = (dataInizio - dataCommissione).days
            if tipo == 'sociale':
                if periodo <= 1:
                    return True
            if tipo == 'commerciale':
                if periodo <= 2:
                    return True
            if tipo == 'mortuaria' or tipo == 'istituzionale':
                return False
        return False
        
    numeroGiorni = fields.Function(fields.Integer('numero giorni'), 'on_change_with_numeroGiorni')    


    @fields.depends('dataInizio', 'dataFine')
    def on_change_with_numeroGiorni(self, name=None):
        if self.dataInizio and self.dataFine:
            ng = abs((self.dataFine - self.dataInizio).days) + 1
            if ng:
                return ng
            return 0

        
    fattoreGiorni = fields.Function(fields.Numeric('fattore giorni', (4,1)), 'on_change_with_fattoreGiorni')    
    @fields.depends(methods = ['numeroGiorni'])
    def on_change_with_fattoreGiorni(self, name=None):
        ng = self.on_change_with_numeroGiorni() #an integer
        if ng:
            return math.ceil((ng - 10) / 5)
        return 0


    
    lines = fields.One2Many('tributi.affissione', 'bolletta', "A string")


    totaleBase = fields.Function(fields.Numeric('totaleBase', (6,4)), 'on_change_with_totaleBase')

    @fields.depends('lines')
    def on_change_with_totaleBase(self, name=None):
        if len(self.lines) > 0:
            tot = 0
            for line in self.lines:
                pass
                tot = tot + line.on_change_with_totaleRigoBase()
            return tot
        return 0

    maggiorazioneNumFogli = fields.Function(fields.Numeric('maggiorazioneNumFogli', (6,4)), 'on_change_with_maggiorazioneNumFogli')

    @fields.depends(methods=['numFogli', 'totaleBase'])
    def on_change_with_maggiorazioneNumFogli(self, name=None):
        numFogli = self.on_change_with_numFogli()
        if numFogli < 50:
            totBase = self.on_change_with_totaleBase()
            newTot = Decimal(totBase) * Decimal(1.5)
            maggiorazione = newTot - Decimal(totBase)
            return round(float(maggiorazione),4)
        return 0


    maggiorazioneUrgenza = fields.Function(fields.Numeric('maggiorazioneUrgenza', (5,4)), 'on_change_with_maggiorazioneUrgenza')

    @fields.depends(methods=['maggiorazioneNumFogli','urgenza', 'totaleBase'])
    def on_change_with_maggiorazioneUrgenza(self, name=None):
        urg  = self.on_change_with_urgenza()
        if urg:
            base = self.on_change_with_totaleBase()
            fogli = self.on_change_with_maggiorazioneNumFogli()
            tot = Decimal(base) + Decimal(fogli)
            newTot = tot * Decimal(1.1)
            maggiorazione = newTot - tot
            return round(float(maggiorazione), 4)
        return 0

    riduzioneTipo = fields.Function(fields.Numeric('riduzioneTipo', (5,4)), 'on_change_with_riduzioneTipo')

    @fields.depends('tipo', methods=['maggiorazioneUrgenza'])
    def on_change_with_riduzioneTipo(self, name=None):
        if self.tipo:
            sommaBase = self.on_change_with_maggiorazioneUrgenza()
            if self.tipo == 'sociale' or self.tipo == 'mortuaria':
                sommaVecchia = sommaBase
                somma = somma * 0.5
                riduzione = sommaVecchia - somma
                return round(riduzione, 4)
            return 0
        return 0
            

    totale = fields.Function(fields.Numeric('totale', (6,4)), 'on_change_with_totale')

    @fields.depends('lines', methods=['totaleBase'])
    def on_change_with_totale(self, name=None):
        if len(self.lines) > 0:
            totBase = self.on_change_with_totaleBase()
            maggiorazioneNumFogli = self.on_change_with_maggiorazioneNumFogli()
            maggiorazioneUrgenza = self.on_change_with_maggiorazioneUrgenza()
            riduzioneTipo = self.on_change_with_riduzioneTipo()
            totale = Decimal(totBase) + Decimal(maggiorazioneNumFogli) + Decimal(maggiorazioneUrgenza) - riduzioneTipo
            return round(float(totale), 4)
        return 0

    
                                    
    pagato = fields.Boolean('pagato ?')
