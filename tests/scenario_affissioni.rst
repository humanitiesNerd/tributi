=============
Tributi Scenario
=============

Imports::

    >>> import os
    >>> import sys
    >>> import unittest
    >>> import doctest
    >>> import re
    >>> import subprocess
    >>> import time
    >>> from itertools import chain
    >>> import operator
    >>> from functools import wraps
    >>> import inspect

    >>> from lxml import etree
    >>> from sql import Table
    >>> import datetime

    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.pool import Pool, isregisteredby
    >>> from trytond import backend
    >>> from trytond.model import Workflow, ModelSQL, ModelSingleton, fields
    >>> from trytond.model.fields import get_eval_fields, Function
    >>> from trytond.tools import is_instance_method
    >>> from trytond.transaction import Transaction
    >>> from trytond.cache import Cache
    >>> from trytond.config import config, parse_uri
    >>> from trytond.wizard import StateView, StateAction
    >>> from trytond.pyson import PYSONDecoder
    >>> from proteus import config, Model, Wizard, Report
    >>> import pdb
    
Install tributi::

    >>> config = activate_modules('tributi')
    
Create parties::

    >>> Party = Model.get('party.party')
    >>> acme = Party(name='Acme')
    >>> acme.save()
    >>> acme.id > 0
    True
    
Create a Bolletta::

    >>> BollettaModel = Model.get('tributi.bolletta')
    >>> bolletta = BollettaModel()
    >>> bolletta.tipo = 'commerciale'
    >>> bolletta.party = acme
    >>> datacommissione = datetime.date(2019, 10, 3)
    >>> datainizio = datetime.date(2019, 10, 4)
    >>> datafine = datetime.date(2019, 10, 16)

    >>> bolletta.dataInizio = datainizio
    >>> bolletta.dataCommissione = datacommissione
    >>> bolletta.dataFine = datafine    
    >>> bolletta.save()
    >>> bolletta.id > 0
    True
    >>> bolletta.dataFine
    datetime.date(2019, 10, 16)
    >>> bolletta.urgenza
    True
    
Create an affissione (bolletta line)::

   >>> affissioneModel = Model.get('tributi.affissione')
   >>> affissione = affissioneModel()
   >>> affissione.formato = '1'
   >>> affissione.numeroFogli = 1
   >>> affissione.categoriaSpeciale = 0
   >>> affissione.quantita = 10
   >>> affissione.titolo = 'Titolo'
   >>> affissione.spaziPrefissati = 0
   >>> affissione.bolletta = bolletta
   >>> bolletta.lines.append(affissione)
   >>> affissione.save()
   >>> bolletta.save()
   >>> int(affissione.spaziPrefissati)
   0

   >>> bolletta.totaleBase
   20.945

   >>> bolletta.totale
   34.5593

   >>> bolletta.maggiorazioneUrgenza
   3.1418

   >>> bolletta.riduzioneTipo
   0

   >>> affissione.totaleRigoBase
   20.945

   >>> affissione.totaleRigo
   20.945

  
   >>> affissione.cinqueggBase
   0.4833

   >>> affissione.prezzoBase
   1.6112

   >>> affissione.importoSpaziPrefissati
   0

   >>> affissione.importoCategoriaSpeciale
   0

   >>> affissione.maggiorazioneM16
   0.0

   >>> affissione.maggiorazioneM12
   0.0



   >>> affissione2 = affissioneModel()
   >>> affissione2.formato = '16'
   >>> affissione2.numeroFogli = 160
   >>> affissione2.quantita = 10
   >>> affissione2.titolo = 'Titolo'
   >>> affissione2.spaziPrefissati = 5
   >>> affissione2.bolletta = bolletta
   >>> bolletta.lines.append(affissione2)
   >>> affissione2.save()
   >>> bolletta.save()
   >>> int(affissione2.spaziPrefissati)
   5
   >>> affissione2.categoriaSpeciale
   0



Create anoher Bolletta::

    >>> BollettaModel = Model.get('tributi.bolletta')
    >>> bolletta2 = BollettaModel()
    >>> bolletta2.tipo = 'commerciale'
    >>> bolletta2.party = acme
    >>> datacommissione = datetime.date(2018, 5, 4)
    >>> datainizio = datetime.date(2018, 5, 7)
    >>> datafine = datetime.date(2018, 6, 13)

    >>> bolletta2.dataInizio = datainizio
    >>> bolletta2.dataCommissione = datacommissione
    >>> bolletta2.dataFine = datafine    
    >>> bolletta2.save()
    >>> bolletta2.id > 0
    True
    >>> bolletta.numeroGiorni
    13
    >>> bolletta.fattoreGiorni
    1
    
Create an affissione (bolletta line)::

   >>> affissioneModel = Model.get('tributi.affissione')
   >>> affissione3 = affissioneModel()
   >>> affissione3.formato = '1'
   >>> affissione3.categoriaSpeciale = 0
   >>> affissione3.quantita = 10
   >>> affissione3.titolo = 'Titolo'
   >>> affissione3.spaziPrefissati = 0
   >>> affissione3.bolletta = bolletta2
   >>> bolletta2.lines.append(affissione3)
   >>> affissione3.save()
   >>> bolletta2.save()
   >>> int(affissione3.spaziPrefissati)
   0

   >>> bolletta2.totaleBase
   125.67

   >>> bolletta2.totale
   188.505

   >>> bolletta2.maggiorazioneUrgenza
   0

   >>> bolletta2.maggiorazioneNumFogli
   62.835

   >>> bolletta2.riduzioneTipo
   0

   >>> affissione3.totaleRigoBase
   125.67

   >>> affissione3.totaleRigo
   125.67

  
   >>> affissione3.cinqueggBase
   0.4833

   >>> affissione3.prezzoBase
   1.6112

   >>> affissione3.importoSpaziPrefissati
   0

   >>> affissione3.importoCategoriaSpeciale
   0

   >>> affissione3.maggiorazioneM16
   0.0

   >>> affissione3.maggiorazioneM12
   0.0

