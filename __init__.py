from trytond.pool import Pool
from bolletta import Bolletta
from affissione import Affissione
__all__ = ['register']


def register():
    Pool.register(
        Bolletta,
        Affissione,
        module='tributi', type_='model')
    Pool.register(
        module='tributi', type_='wizard')
    Pool.register(
        module='tributi', type_='report')
