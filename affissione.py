from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool
from trytond.pyson import Eval, If, PYSONEncoder
import math
import datetime
#import ipdb
from decimal import Decimal

__all__ = ['Affissione']



class Affissione(ModelSQL, ModelView):
    'Affissione'
    __name__ = 'tributi.affissione'

    bolletta = fields.Many2One('tributi.bolletta', 'Bolletta', ondelete='CASCADE',
        select=True)
    

    bollettaDataCommissione = fields.Function(fields.Date('Data Commissione'), 'on_change_with_bollettaDataCommissione')
    bollettaDataInizio = fields.Function(fields.Date('Data Inizio'), 'on_change_with_bollettaDataInizio')
    bollettaDataFine = fields.Function(fields.Date('Data Fine'), 'on_change_with_bollettaDataFine')
    bollettaTipo = fields.Function(fields.Selection([('commerciale', 'Commerciale'),('altro', 'Altro')], 'tipo', states={'invisible': (Eval('bollettaTipo') != 'commerciale')}), 'on_change_with_bollettaTipo')

    fattoreGiorni = fields.Function(fields.Numeric('fattore giorni', (4,1)), 'on_change_with_fattoreGiorni')    


    @fields.depends('bolletta')
    def on_change_with_fattoreGiorni(self, name=None):
        if self.bolletta:
            return self.bolletta.fattoreGiorni
        return Decimal(0)
    

    @fields.depends('bolletta')
    def on_change_with_bollettaTipo(self, name=None):
        if self.bolletta:
            return self.bolletta.tipo
        return 'altro'
    
    @fields.depends('bolletta')
    def on_change_with_bollettaDataCommissione(self, name=None):
        if self.bolletta:
            return self.bolletta.dataCommissione
        return datetime.date.today()

    @fields.depends('bolletta')
    def on_change_with_bollettaDataInizio(self, name=None):
        if self.bolletta:
            return self.bolletta.dataInizio
        return datetime.date.today()

    @fields.depends('bolletta')
    def on_change_with_bollettaDataFine(self, name=None):
        if self.bolletta:
            return self.bolletta.dataFine
        return datetime.date.today()

    titolo = fields.Char('Titolo', required=True)

    quantita = fields.Integer('Quantità', required=True)

    formato = fields.Selection([
        ('1', '1'),
        ('2', '2'),
        ('4', '4'),
        ('8', '8'),
        ('12','12'),
        ('16', '16'),
        ('24', '24')
    ], 'formato', required=True)

    
    numeroFogli = fields.Function(fields.Integer('Numero Fogli'), 'on_change_with_numeroFogli')

    @fields.depends('formato', 'quantita')
    def on_change_with_numeroFogli(self, name=None):
        if self.formato and self.quantita:
            return int(self.formato) * self.quantita
        return 0
    
    prezzoBaseUnFoglioDa1A10gg = 1.6112
    prezzoBasePiuUnFoglioDa1A10gg = 2.014

    @staticmethod
    def base_per_spazi_prefissati_e_categoria_speciale(parametro, spaziPrefissatiOCatSpeciale, formato, prezzoBase, cinqueggBase, fattoreGiorni):
        step0 = Decimal(cinqueggBase) * fattoreGiorni
        step1 = spaziPrefissatiOCatSpeciale * parametro * formato * prezzoBase
        step2 = Decimal(step1) + step0
        return step2

    
    cinqueggBase = fields.Function(fields.Numeric('cinque giorni base', (1,4)), 'on_change_with_cinqueggBase')    
    @fields.depends('formato')
    def on_change_with_cinqueggBase(self, name=None):
        if self.formato is None:
            return 0
        elif self.formato == '1':
            return 0.4833
        else:
            return 0.6042
        
    #ipdb.set_trace(context=5)

    prezzoBase = fields.Function(fields.Numeric('prezzo base', (1,4)), 'on_change_with_prezzoBase')
    
    @fields.depends('formato')
    def on_change_with_prezzoBase(self, name=None):
        if self.formato is None:
            return 0
        elif self.formato == '1':
            return self.prezzoBaseUnFoglioDa1A10gg
        else:
            return self.prezzoBasePiuUnFoglioDa1A10gg

    spaziPrefissati = fields.Integer('Spazi Prefissati', required=True,
                                     domain=[('spaziPrefissati', '<=', Eval('quantita', None)),
                                             ('spaziPrefissati', '>=', 0)],
                                     depends= ['quantita'])

    importoSpaziPrefissati = fields.Function(fields.Numeric('Importo Spazi Prefissati', (6,4)), 'on_change_with_importoSpaziPrefissati')

    @fields.depends('formato', 'spaziPrefissati', methods=['prezzoBase', 'cinqueggBase', 'fattoreGiorni'])
    def on_change_with_importoSpaziPrefissati(self, name=None):
        if self.spaziPrefissati:
            return Affissione.base_per_spazi_prefissati_e_categoria_speciale(
                2,
                self.spaziPrefissati, int(self.formato),
                self.on_change_with_prezzoBase(),
                self.on_change_with_cinqueggBase(),
                self.on_change_with_fattoreGiorni())
        return 0


    @fields.depends('categoriaSpeciale', 'formato', 'spaziPrefissati', methods=['prezzoBase', 'cinqueggBase', 'fattoreGiorni'])
    def on_change_with_importoCategoriaSpeciale(self, name=None):
        if self.categoriaSpeciale:
            return Affissione.base_per_spazi_prefissati_e_categoria_speciale(
                2.5, self.categoriaSpeciale, int(self.formato), self.on_change_with_prezzoBase(), self.on_change_with_cinqueggBase(), self.on_change_with_fattoreGiorni())
        return 0

    categoriaSpeciale = fields.Integer('Categoria Speciale',
                                     states={'invisible': (Eval('bollettaTipo') != 'commerciale')},
                                     domain=[('categoriaSpeciale', '<=', Eval('quantita', None)),
                                             ('categoriaSpeciale', '>=', 0)
                                     ],
                                     depends= ['quantita'])

    
    @staticmethod
    def default_categoriaSpeciale():
        return 0


    @staticmethod
    def default_spaziPrefissati():
        return 0

    
    importoCategoriaSpeciale = fields.Function(fields.Numeric('importo categoria speciale', (6,4),
                                                            states={'invisible': (Eval('bollettaTipo') != 'commerciale')}
    ),
                                               'on_change_with_importoCategoriaSpeciale')

        
    totaleRigoBase = fields.Function(fields.Numeric('totale rigo base', (6,4)), 'on_change_with_totaleRigoBase')

    @fields.depends(methods =
                    ['bollettaTipo',
                     'prezzoBase',
                     'cinqueggBase',
                     'fattoreGiorni', 
                     'importoSpaziPrefissati',
                     'numeroFogli',
                     'importoCategoriaSpeciale'
                      ])

    def on_change_with_totaleRigoBase(self, name=None):

        bollettaTipo = self.on_change_with_bollettaTipo()
        if bollettaTipo == 'istituzionale':
            return 0

        prezzoBase = self.on_change_with_prezzoBase()      
        cinqueggBase = self.on_change_with_cinqueggBase()
        fattoreGiorni = self.on_change_with_fattoreGiorni()
        importoSpaziPrefissati = self.on_change_with_importoSpaziPrefissati()
        numeroFogli = self.on_change_with_numeroFogli()
        importoCatSpeciale = self.on_change_with_importoCategoriaSpeciale()
        step1 = prezzoBase + cinqueggBase
        step2 = Decimal(step1) * fattoreGiorni
        
        step3 = step2 * numeroFogli
        step4 = importoSpaziPrefissati + importoCatSpeciale
        step5 = step3 + step4
        return round(float(step5), 4)
        #somma = (prezzoBase + cinqueggBase * fattoreGiorni ) * numeroFogli +
        #importoSpaziPrefissati + importoCatSpeciale
        #return round(somma, 4)



    maggiorazioneM16 = fields.Function(fields.Numeric('maggiorazioneM16', (5,4)), 'on_change_with_maggiorazioneM16' )

    @fields.depends('formato', methods=['totaleRigoBase'])
    def on_change_with_maggiorazioneM16(self, name=None):
        if self.formato:
            somma = self.on_change_with_totaleRigoBase()
            sommaVecchia = somma
            if int(self.formato) >= 16:
                somma = somma * 2
                maggiorazione = somma - sommaVecchia
                return Decimal(maggiorazione)
            return 0.0
        return Decimal(0.0)

    maggiorazioneM12 = fields.Function(fields.Numeric('maggiorazioneM12', (5,4)), 'on_change_with_maggiorazioneM12')

    @fields.depends('formato', methods=['maggiorazioneM16'])
    def on_change_with_maggiorazioneM12(self, name=None):
        if self.formato:
            somma = self.on_change_with_maggiorazioneM16()
            sommaVecchia = somma
            if 8 <= int(self.formato) <= 12:
                somma = somma * 1.5
                maggiorazione = somma - sommaVecchia
                return Decimal(maggiorazione)
            return 0.0
        return Decimal(0.0)
                
    totaleRigo = fields.Function(fields.Numeric('totale rigo', (6,4)), 'on_change_with_totaleRigo')

    @fields.depends(methods=['totaleRigoBase', 'maggiorazioneM12', 'maggiorazioneM16'])
    def on_change_with_totaleRigo(self, name=None):
        totBase = self.on_change_with_totaleRigoBase()
        maggiorazioneM12 = self.on_change_with_maggiorazioneM12()
        maggiorazioneM16 = self.on_change_with_maggiorazioneM16()
        totale = Decimal(totBase) + Decimal(maggiorazioneM12) + Decimal(maggiorazioneM16)
        return round(float(totale), 4)
        
